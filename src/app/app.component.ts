import { Component } from '@angular/core';

interface Link {
  name: string;
}

type Links = Link[];

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'pahk-rugs';

  links: Links = [
    { name: 'New Arrivals' },
    { name: 'All Rugs' },
    { name: 'Rugs by Size' },
    { name: 'Rugs by Colour' },
    { name: 'Rugs by Store' },
  ];
}
