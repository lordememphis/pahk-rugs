/** @type {import('tailwindcss').Config} */

const theme = require("tailwindcss/defaultTheme");

module.exports = {
  content: ["./src/**/*.{html,ts}"],
  theme: {
    extend: {
      fontFamily: {
        sans: ["Work Sans", ...theme.fontFamily.sans],
      },
    },
  },
  plugins: [],
};
